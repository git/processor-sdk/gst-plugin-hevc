/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _GSTH265DECBUF_
#define _GSTH265DECBUF_

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/
#include <gst/gst.h>
#include "gsth265dec.h"
#include "gsth265utils.h"

/*******************************************************************************
*   MACROS AND STRUCTURES
*******************************************************************************/
/** @defgroup H265_DECODER
*/
/** Define to indicate FALSE
*/
#define PP_FALSE    0
/** Define to indicate TRUE
*/
#define PP_TRUE     1

/** Maximum reference frames
* Maximum reference frames this decoder configuration supports.
* @ingroup H265_DECODER
*/
#define FILE_NAME_SIZE 512
#define PP_H265DEC_MAX_MEMTAB   70
#define PP_H265VDEC_MAX_REF_FRAMES 16

/** Set status parameters macros*/
#define PP_SET_DATASYNC             1
#define PP_SET_DPB_FLUSH            2
#define PP_SET_RES_INV              3
#define PP_SET_DECODE_HDR           4

/** Get status parameters macros*/
#define PP_GET_PARAMSTATUS          1
#define PP_GET_BUFSTATUS            2
#define PP_GET_ERRORSTATUS          3

/** Pic width and height*/
#define PP_H265_MAX_PIC_WIDTH        4096
#define PP_H265_MAX_PIC_HEIGHT       2176
 /* Macro to set Error bits */
#define PP_H265_SET_ERROR_BIT(x, m)  ((x) |= (0x1 << m))

#define MAX_BUFF_ELEMENTS 17
#define DPB_MAXLUMAPS 8912896  /*Value for level 5.2 from Table A-1*/

/**
 *  @brief      Status of the buffer elements.
 */
typedef enum {
    BUFFMGR_FREE = 0,
    BUFFMGR_USED = 1
} BUFFMGR_BufferStatus;

/**
 *  @brief      Definition of buffer element in the buffer array.
 *            every time a new buffer is requested buffer manager
 *            module returns a pointer to one of the elements
 *            defined below from the buffer array
 */
typedef struct BuffEle
{
    /** Unique Id which identifies the buffer element */
    tPPu32 bufId;
    /**
     * Status of the buffer element: can be either free for allocation or
     * held by the codec and cannot be allocated
     */
    BUFFMGR_BufferStatus bufStatus;
    /** Size of buffer members held by this buffer element in bytes */
    tPPu32 bufSize[3];
    /** buffer members which contain the address of the actual buffers
     * represented by this buffer element */
    tPPu8 *buf[3];
} BUFFMGR_buffEle;

typedef struct PPInGst_Buff
{
 /*! Unique Id which identifies the buffer element */
    tPPi32 bufId;
    BUFFMGR_BufferStatus bufStatus;
    GstBuffer *GstDispBuf;
    GstMapInfo Out_map;
}tPPInGst_Buff;

typedef BUFFMGR_buffEle* BUFFMGR_buffEleHandle;

BUFFMGR_buffEle buffArray[MAX_BUFF_ELEMENTS];


/*******************************************************************************
*   FUNCTION PROTOTYPES
*******************************************************************************/

/**
 ******************************************************************************
 * @fn gPP_H265_QueryMemoryRequirements (tPPQueryMemRecords  *apQueryMemRecords,
 *                             tPPH265BPD_CreateParams *apCreateParams,
 *                             tPPu32 *apNumMemTabEntries);
 * @ingroup H265_DECODER
 *
 * @brief This function queries for H.265 video decoder object
 *              memory requirements.
 *
 * @param apQueryMemRecords  [IN/OUT] Pointer to memory query records
 * @param apCreateParams     [IN] Pointer to decoder create parameter structure
 * @param apNumMemTabEntries [OUT] Number of MemTab Entries
 *
 * @return None
 ******************************************************************************
 */

tPPResult gPP_H265_QueryMemoryRequirements(
                             tPPQueryMemRecords    *apQueryMemRecords,
                             tPP_H265_CreateParams *apCreateParams,
                             tPPu32                *apNumMemTabEntries);

/**
******************************************************************************
* @fn MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  *apQueryMemRecords,
*                                         tPPu32 aNumMemTabEntries)
*
*
* @brief This function is a utility function used to allocate memory
*         as per the modules request
*
*
* @param apQueryMemRecords [IN/OUT] Pointer to memory query records
*
* @param aNumMemTabEntries [OUT] Number of Mem Tab Entries
*
* @return Status code - SC_PP_SUCCESS       : if success
*                       EC_PP_OUT_OF_MEMORY : insufficient memory status
******************************************************************************
*/

tPPResult MEMMGR_AllocMemoryRequirements (
  tPPQueryMemRecords  *apQueryMemRecords, tPPu32 aNumMemTabEntries);

/**
******************************************************************************
* @fn MEMMGR_DeAllocMemory (tPPBaseDecoder *apBase)
*
* @brief This function destroys H.265 video decoder object
*
* @param appBase        [IN] Pointer to decoder object
*
* @return Status code - SC_PP_SUCCESS if success, or the following error codes
*                       EC_PP_FAILURE : General failure
*
******************************************************************************
*/
tPPResult MEMMGR_DeAllocMemory (tPPQueryMemRecords  *apQueryMemRecords);
/*****************************************************************************/
/**
* @fn tPPi32 BUFFMGR_GetFreeBufID(tPPInGst_Buff *GstBufOut)
*
* @brief  Implementation of buffer manager get free buffer module.
*         The BUFFMGR_GetFreeBuffer function searches for a free buffer in the
*         global buffer array and returns the address of that element. Incase
*         if none of the elements are free then it returns NULL
*
*
* @return Valid buffer element address or NULL incase if no buffers are empty
*
*/
/*****************************************************************************/
tPPi32 BUFFMGR_GetFreeBufID(tPPInGst_Buff *GstBufOut);
/*****************************************************************************/
/**
* @fn tPPi32 BUFFMGR_ReleaseBufID(tPPi32 bufffId[], tPPInGst_Buff *GstBufOut)
*
* @brief  Implementation of buffer manager
*       release module
*
*        The BUFFMGR_ReleaseBuffer function takes an array of buffer-ids
*        which are released by the test-app. "0" is not a valid buffer Id
*        hence this function keeps moving until it encounters a buffer Id
*        as zero or it hits the MAX_BUFF_ELEMENTS
*
*
* @return None
*
*/
/*****************************************************************************/

tPPi32 BUFFMGR_ReleaseBufID(tPPi32 bufffId[], tPPInGst_Buff *GstBufOut);

#endif /*_GSTH265DECBUF_*/


