/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __GST_H265DEC_H__
#define __GST_H265DEC_H__

#include <gst/gst.h>

G_BEGIN_DECLS


#define GST_TYPE_H265DEC \
  (gst_h265dec_get_type())
#define GST_H265DEC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_H265DEC,Gsth265dec))
#define GST_H265DEC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_H265DEC,Gsth265decClass))
#define GST_IS_H265DEC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_H265DEC))
#define GST_IS_H265DEC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_H265DEC))
#define GST_H265DEC_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_H265DEC, Gsth265decClass))  

#define FUNC_MEMSET gH265D_Memset
/* NAL UNIT TYPES */
#define H265_TRAIL_N_NALUNIT    0
#define H265_TRAIL_R_NALUNIT    1
#define H265_TSA_N_NALUNIT      2
#define H265_TLA_NALUNIT        3
#define H265_STSA_N_NALUNIT     4
#define H265_STSA_R_NALUNIT     5
#define H265_RADL_N_NALUNIT     6
#define H265_RADL_R_NALUNIT     7
#define H265_RASL_N_NALUNIT     8
#define H265_RASL_R_NALUNIT     9
#define H265_BLA_W_LP_NALUNIT   16
#define H265_BLA_W_RADL_NALUNIT 17
#define H265_BLA_N_LP_NALUNIT   18
#define H265_IDR_W_RADL_NALUNIT 19
#define H265_IDR_N_LP_NALUNIT   20
#define H265_CRA_NALUNIT        21
#define H265_VPS_NALUNIT        32
#define H265_SPS_NALUNIT        33
#define H265_PPS_NALUNIT        34
#define H265_AUDEL_NALUNIT      35
#define H265_EOSEQ_NALUNIT      36
#define H265_EOSTRM_NALUNIT     37
#define H265_FILDATA_NALUNIT    38
#define H265_SEI_NALUNIT_PREFIX 39
#define H265_SEI_NALUNIT_SUFFIX 40
#define H265_NEW_SLICE_FOUND    31
#define H265_MAX_SLICES_PER_FRAME    (201)
#define H265_BYTESTRM_PREFIX          (1)
#define SLICE_HDR_NALU          200
#define SC_PP_NEXT_FRAME_FOUND        (9)
/* The below macro will be replaced by "rev" instruction by the compiler */
#define SWAP_BYTE(word) ((word<<24) | (word>>24) | ((word&0xFF00)<<8)\
                        | ((word&0xFF0000)>>8))


extern gint32  gPP_PPL_AtomicAdd8(guint8 *ptr, guint8 val);

extern void gH265D_Memset(void *apSrc, gint32 aValue, gint32 aNumBytes);

typedef struct _Gsth265dec      Gsth265dec;
typedef struct _Gsth265decClass Gsth265decClass;
/**
 *  @brief      Definition of NAL unit parameters
 */
typedef struct tPPGst_NALStruct
{
    /** For parsing NAL unit*/
    gint32 NAlUnitType[300];
    /** NAL ID **/
    gint32 NALId;
    /**  New Frame flag **/
    gint32 NewFrameFound;
}tPPGstNAL;

struct _Gsth265dec
{
    GstElement element;
    GstPad *sinkpad, *srcpad;
    gboolean silent;
    guint32 framerate ;
    guint32 duration;
    gint64  nTimeStamp;
    guint32 nFileRead;
    guint32 nNoBytes;
    guint32 nByteRead ;
    guint32 nByteLeft;
    guint32 nIndex;
    guint32 nBytesDec;
    guint32 nBufFilled ;
    guint32 nBytesConsumed ;
    guint32 nTOTAL_FRAME_SIZE;
    guint32 nLumSize;
    guint32 nCbSize;
    guint32 nCrSize;
    gint32  nRetVal  ;
    gint32  nInitDone;
    gint32  nAllocDone ;
    guint32 nCurrThreadID ;
    gint32  nRetV;
    guint32 nNumMemTabEntries ;
    gint32  nResult ;
    guint32 taskid;
    gint width;
    gint height;
    gint threads;
    guint nInitFlag;
    GstBufferPool *externalpool;
 };

struct _Gsth265decClass 
{
  GstElementClass parent_class;
};

GType gst_h265dec_get_type (void);

G_END_DECLS

#endif /* __GST_H265DEC_H__ */
