/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/
#include "gsth265buf.h"
#include<stdio.h>
#include<stdlib.h>
/*******************************************************************************
*   FUNCTION DEFINITIONS
*******************************************************************************/

/**
******************************************************************************
* @fn MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  *apQueryMemRecords,
*                                         tPPu32 aNumMemTabEntries)
*
*
* @brief This function is a utility function used to allocate memory
*         as per the modules request
*
*
* @param apQueryMemRecords [IN/OUT] Pointer to memory query records
*
* @param aNumMemTabEntries [OUT] Number of Mem Tab Entries
*
* @return Status code - SC_PP_SUCCESS       : if success
*                       EC_PP_OUT_OF_MEMORY : insufficient memory status
******************************************************************************
*/
tPPResult MEMMGR_AllocMemoryRequirements (tPPQueryMemRecords  \
                                          *apQueryMemRecords,
                                          tPPu32 aNumMemTabEntries)
{
    tPPu32 nMemTabIdx = 0;

    for(nMemTabIdx = 0; nMemTabIdx < aNumMemTabEntries; nMemTabIdx++)
    {
        apQueryMemRecords[nMemTabIdx].allotedHandle =
            (tPPu8 *) malloc(apQueryMemRecords[nMemTabIdx].reqSize);

        if (apQueryMemRecords[nMemTabIdx].allotedHandle == NULL)
        {
            return EC_PP_OUT_OF_MEMORY;
        }
    }
    return SC_PP_SUCCESS;
}

/**
******************************************************************************
* @fn MEMMGR_DeAllocMemory (tPPBaseDecoder *apBase)
*
* @brief This function destroys H.265 video decoder object
*
* @param appBase        [IN] Pointer to decoder object
*
* @return Status code - SC_PP_SUCCESS if success, or the following error codes
*                       EC_PP_FAILURE : General failure
*
******************************************************************************
*/
tPPResult MEMMGR_DeAllocMemory (tPPQueryMemRecords  *apQueryMemRecords)
{
    tPPi32 nIndex;

    if(apQueryMemRecords == NULL)
    {
        return EC_PP_FAILURE;
    }

    for(nIndex = PP_H265DEC_MAX_MEMTAB-1; nIndex >= 0; nIndex--)
    {
        if(apQueryMemRecords[nIndex].allotedHandle != NULL)
        {
            free(apQueryMemRecords[nIndex].allotedHandle);
            apQueryMemRecords[nIndex].allotedHandle = NULL;
        }
    }
    return SC_PP_SUCCESS;
}/* MEMMGR_DeAllocMemory */

/*****************************************************************************/
/**
* @fn tPPi32 BUFFMGR_GetFreeBufID(tPPInGst_Buff *GstBufOut)
*
* @brief  Implementation of buffer manager get free buffer module.
*         The BUFFMGR_GetFreeBuffer function searches for a free buffer in the
*         global buffer array and returns the address of that element. Incase
*         if none of the elements are free then it returns NULL
*
*
* @return Valid buffer element address or NULL incase if no buffers are empty
*
*/
/*****************************************************************************/
tPPi32 BUFFMGR_GetFreeBufID(tPPInGst_Buff *GstBufOut)
{

    tPPu32 tmpCnt = 0;
    for(tmpCnt = 0; tmpCnt < MAX_BUFF_ELEMENTS; tmpCnt++)
    {
        if(GstBufOut[tmpCnt].bufStatus == BUFFMGR_FREE)
        {
            GstBufOut[tmpCnt].bufId  = tmpCnt+1;
            GstBufOut[tmpCnt].bufStatus = BUFFMGR_USED;
            return (tmpCnt+1) ;
        }
    }
    printf("Run short of frames !!\n");
    return 0;
}

/*****************************************************************************/
/**
* @fn tPPi32 BUFFMGR_ReleaseBufID(tPPi32 bufffId[], tPPInGst_Buff *GstBufOut)
*
* @brief  Implementation of buffer manager
*       release module
*
*        The BUFFMGR_ReleaseBuffer function takes an array of buffer-ids
*        which are released by the test-app. "0" is not a valid buffer Id
*        hence this function keeps moving until it encounters a buffer Id
*        as zero or it hits the MAX_BUFF_ELEMENTS
*
*
* @return None
*
*/
/*****************************************************************************/

tPPi32 BUFFMGR_ReleaseBufID(tPPi32 bufffId[], tPPInGst_Buff *GstBufOut)
{

    tPPu32 tmpCnt, tmpId;

    for(tmpCnt = 0;
        (tmpCnt < MAX_BUFF_ELEMENTS);
        tmpCnt++)
    {
        tmpId = bufffId[tmpCnt];

        /*
        * Check if the buffer Id = 0 condition has reached. zero is not a
        * valid buffer Id hence that value is used to identify the end of
        * buffer array
        */
        if(tmpId == 0)
        {
            break;
        }
        /*
        * convert the buffer-Id to its corresponding index in the global
        * array
        */
        tmpId--;

        if(tmpId >= MAX_BUFF_ELEMENTS) {

            /* Inidcates an invalid buffer Id passed - this buffer Id can be
            * ignored!! alternatively we can break here.
            */
            continue;
        }

        if(GstBufOut[tmpId].bufStatus == BUFFMGR_FREE) {
            /* Trying to release an already free bufffer this idicates some
            *  mismanagement in buffer usage following printf will help
            * application developer to identify any such problem in her
            * algorithm
            */
            g_print("\n ......Trying to release already freed buffer......\n");
        }
        else
        {
            /* Set the status of the buffer to FREE */
            GstBufOut[tmpId].bufStatus = BUFFMGR_FREE;
            gst_buffer_unmap(GstBufOut[tmpId].GstDispBuf, &GstBufOut[tmpId].Out_map);
            gst_buffer_unref( GstBufOut[tmpId].GstDispBuf);
        }
    }
    return 0;
}

