
/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*******************************************************************************
*   INCLUDE FILES
*******************************************************************************/
#include "gsth265utils.h"
#include<stdio.h>
#include<stdlib.h>
/*******************************************************************************
*   FUNCTION DEFINITIONS
*******************************************************************************/

/**
******************************************************************************
* @fn tPPResult sH265_FindSyncMarker (tPPGstNAL *aGstNAL,
*                                     tPPu8  *apBitStream,
*                                     tPPu32 aBitStreamLen,
*                                     tPPu32 *apSyncIndex,
*                                     tPPu32 nPrefixFlag)
*
* @brief This function is used to set find the byte-aligned sync marker in
*        the bit-stream, from the given point.
*
* @param aGstNAL         [IN] Structure holding NAL unit details
* @param apBitStream     [IN] Bit stream data which is to be decoded.
* @param apBitStreamLen  [IN/OUT] Length of the bitstream.
* @param apSyncIndex     [IN/OUT] Length at which the bitstream prefix is
*                                 found from the begining of the stream
* @param nPrefixFlag     [IN] Prefix Flag
* @return Status code
*      SC_PP_SUCCESS  : Sync found and its position is given back
*      EC_PP_FAILURE        : Sync not found
*      EC_PP_INVALID_PARAM  : NULL params
******************************************************************************
*/
tPPResult sH265_FindSyncMarkerFrame(tPPGstNAL *aGstNAL,
                                    tPPu8  *apBitStream,
                                    tPPu32 aBitStreamLen,
                                    tPPu32 *apSyncIndex,
                                    tPPu32 nPrefixFlag)
{
    tPPu32 nWord;
    tPPu8 *pLocalBitStrmPtr = apBitStream;
    tPPu8 *pLocalMaxBitStrmPtr = apBitStream + aBitStreamLen;
    tPPu32 nIndex;
    tPPi32 nTemp;
    tPPi32 nTemp1;
    tPPi32 nNalUnitType;
    tPPi32 nFirstSliceInPicFlag;

    nWord = ((*pLocalBitStrmPtr++) << 16);
    nWord += ((*pLocalBitStrmPtr++) << 8);
    nWord += (*pLocalBitStrmPtr++);

    nIndex = 3;
    /*
    * Check if the formed word is nothing but the sync word  -
    * else check if end-of bit-stream has been reached
    */
    while((nWord != H265_BYTESTRM_PREFIX) &&
        (pLocalBitStrmPtr < pLocalMaxBitStrmPtr))
    {
        /*
        * The word is checked for 0x00000003, This is Emulation Prevention
        * Byte removal
        */
        if(nWord == 0x000003)
        {
            apBitStream++;
            apBitStream++;
            nWord = ((*pLocalBitStrmPtr++) << 16);
            nWord += ((*pLocalBitStrmPtr++) << 8);
            nWord += (*pLocalBitStrmPtr++);
            nIndex = nIndex + 3;
        }

        else
        {
            nWord <<= 8;
            apBitStream++;
            nWord &= 0x00FFFFFF;
            nWord += (*pLocalBitStrmPtr++);
            nIndex++;
        }
    }

    if((nWord == H265_BYTESTRM_PREFIX) && nPrefixFlag)
    {
        /* if((nWord & 0x00ffffff) == 0x00010000)*/
        nTemp = SWAP_BYTE((*(tPPu32 *)pLocalBitStrmPtr));
        nTemp1 = (nTemp >> 16);
        /* forbidden zero bit is always zero */
        if(nTemp1 & 0x8000)
        {
            /* This bit is supposed to be zero incase if it is not
            *   then log a debug message accordingly
            */
            nTemp1 = nTemp1 & 0x7FFF;
        }

        /* NAL unit type, nal_unit_type*/
        nNalUnitType = (nTemp1 >> 9) & 0x3F;

        aGstNAL->NAlUnitType[aGstNAL->NALId] = nNalUnitType;

        switch(nNalUnitType)
        {
        case H265_IDR_W_RADL_NALUNIT:
        case H265_IDR_N_LP_NALUNIT:
        case H265_TRAIL_N_NALUNIT:
        case H265_TRAIL_R_NALUNIT:
        case H265_TSA_N_NALUNIT:
        case H265_TLA_NALUNIT:
        case H265_STSA_N_NALUNIT:
        case H265_STSA_R_NALUNIT:
        case H265_RADL_N_NALUNIT:
        case H265_RADL_R_NALUNIT:
        case H265_RASL_N_NALUNIT:
        case H265_RASL_R_NALUNIT:
        case H265_BLA_W_LP_NALUNIT:
        case H265_BLA_W_RADL_NALUNIT:
        case H265_BLA_N_LP_NALUNIT:
        case H265_CRA_NALUNIT:

            {
                nFirstSliceInPicFlag = (nTemp >> 15) & 0x1;

                aGstNAL->NAlUnitType[aGstNAL->NALId] = SLICE_HDR_NALU;

                if(aGstNAL->NALId > 0)
                {
                    if((aGstNAL->NAlUnitType[aGstNAL->NALId -1]\
                        ==  SLICE_HDR_NALU) && nFirstSliceInPicFlag)
                    {
                        aGstNAL->NewFrameFound = 1;
                        return SC_PP_NEXT_FRAME_FOUND;
                    }

                    if((aGstNAL->NAlUnitType[aGstNAL->NALId -1]\
                        ==  H265_SEI_NALUNIT_SUFFIX) && nFirstSliceInPicFlag)
                    {
                        aGstNAL->NewFrameFound = 1;
                        return SC_PP_NEXT_FRAME_FOUND;
                    }
                }
                break;
            }
        default:
            {
                if(aGstNAL->NALId > 0)
                {
                    if(aGstNAL->NAlUnitType[aGstNAL->NALId - 1]
                    == SLICE_HDR_NALU)
                    {
                        if(aGstNAL->NAlUnitType[aGstNAL->NALId]
                        != H265_SEI_NALUNIT_SUFFIX)
                        {
                            aGstNAL->NewFrameFound = 1;
                            return SC_PP_NEXT_FRAME_FOUND;
                        }
                    }
                    if(aGstNAL->NAlUnitType[aGstNAL->NALId - 1]
                    == H265_SEI_NALUNIT_SUFFIX)
                    {
                        if(aGstNAL->NAlUnitType[aGstNAL->NALId]
                        != H265_SEI_NALUNIT_SUFFIX)
                        {
                            aGstNAL->NewFrameFound = 1;
                            return SC_PP_NEXT_FRAME_FOUND;
                        }
                    }
                }
                break;
            }
        }/*switch(nNalUnitType)*/
    }/*if((nWord == H265_BYTESTRM_PREFIX) && nPrefixFlag)*/

    if(nIndex>aBitStreamLen)
    {
        nIndex = aBitStreamLen;
    }

    if(nWord == H265_BYTESTRM_PREFIX)
    {
        *apSyncIndex = nIndex - 3;
        return SC_PP_SUCCESS;
    }

    if(pLocalBitStrmPtr == pLocalMaxBitStrmPtr)
    {
        apBitStream++ ;
        apBitStream++ ;
        apBitStream++ ;
    }

    if(nIndex == aBitStreamLen)
    {
        *apSyncIndex = nIndex;
        return SC_PP_SUCCESS;
    }

    /*
    * Since no sync was not found - to re-search with more bytes user
    * would requires to re-provide last two bytes again - to facilitate
    * this operation last two bytes is not consumed here.
    */
    *apSyncIndex = (aBitStreamLen - 2);
    return EC_PP_H265_NO_SYNC_FOUND;
}
/**
******************************************************************************
* @fn tPPResult sH265_FindNalUFrame (tPPGstNAL *aGstNAL,
*                              tPPu8  *apBitStream,
*                              tPPi32 *apBitStreamLen,
*                              tPPu32 *apPrefix,
*                              tPPu32 *apSuffix)
*
* @brief This function detects the boundaries of a NAL unit and gives
*        the starting and ending indices in the bitstream, if complete NAL
*        unit is not found either due to no sync-mark at the end or at the
*        begining it returns error.
* @param   aGstNAL        [IN] NAL unit structure
* @param  apBitStream     [IN] Bit stream data which is to be decoded.
* @param  apBitStreamLen  [IN/OUT] Length of the bitstream.
* @param  apPrefix        [IN] Prefix
* @param  apSuffix        [IN] Suffix
* @return Status code
*
*      SC_PP_SUCCESS  : Both the Sync found and its position is given back
*      EC_PP_H265_NO_SYNC_FOUND: Sync not found
*      EC_PP_INSUFFICIENT_BUFFER  : Buffer length is not sufficient to detect
*                          Sync points. If the first sync marker is found but
*                          second sync marker is not found then this
******************************************************************************
*/
tPPResult sH265_FindNalUFrame (tPPGstNAL *aGstNAL,
                               tPPu8  *apBitStream,
                               tPPi32 *apBitStreamLen,
                               tPPu32 *apPrefix,
                               tPPu32 *apSuffix)
{

    tPPi32 nRetVal = SC_PP_SUCCESS;

    /*---------------------------------------------------------------*/
    /* Find Prefix length before the sync marker of present NAL unit */
    /* Atleast 4 bytes is necessary 3 - for sync and 1 for NAL-header*/
    /*---------------------------------------------------------------*/
    if(*apBitStreamLen  > 4)
    {
        /* Get Prefix - this excludes the sync-marker bytes in the return
        values */
        nRetVal = sH265_FindSyncMarkerFrame(aGstNAL, apBitStream,
            *apBitStreamLen, apPrefix, 1);

        if(nRetVal == SC_PP_NEXT_FRAME_FOUND)
        {
            return nRetVal;
        }
        if(nRetVal != SC_PP_SUCCESS)
        {
            /*
            * Note that FindSync marker gives number of bytes consumed till
            * now - to facilitate re-searching of sync marker with more number
            * of bytes in the next call
            */
            (*apBitStreamLen) = (*apBitStreamLen) - *apPrefix;
            return EC_PP_H265_NO_NALU_START_FOUND;
        }

    }
    else
    {
        return EC_PP_INSUFFICIENT_BUFFER;
    }
    /*
    * Find length of the present NAl-unit which extends till the
    * Sync marker of next NAL unit
    */
    if(*apBitStreamLen - (*apPrefix + 3) > 1)
    {
        /* Start search for next sync marker */

        nRetVal = sH265_FindSyncMarkerFrame(aGstNAL ,
            apBitStream + *apPrefix + 3, (*apBitStreamLen) - (*apPrefix + 3),
            apSuffix, 0);

        /* Compensate 3-byte offset introduced in the search by adding 3 */
        *apSuffix += 3;

        if(nRetVal != SC_PP_SUCCESS)
        {
            /*
            * Note that FindSync marker gives number of bytes consumed till
            * now - to facilitate re-searching of sync marker with more number
            * of bytes in the next call
            */
            (*apBitStreamLen) = (*apBitStreamLen) - (*apSuffix + *apPrefix);
            /*
            * EXPLAIN: here return value of FindSyncMarker function is discarded
            * and No NALU end in sight is reported - this indicates to the user
            * that starting sync marker was found but end could not be
            * determined with confidence.
            */
            return EC_PP_H265_NO_NALU_END_FOUND;
        }
    }
    else
    {
        *apBitStreamLen = (*apBitStreamLen - *apPrefix);
        return EC_PP_INSUFFICIENT_BUFFER;
    }
    return nRetVal;
}
/**
******************************************************************************
* @fn tPPResult gH265_ParseNALUnitFrame( tPPGstNAL *pGstNAL,
*                                        tPPInput_BitStream  H265InputStreamNAL,
*                                        tPPi32 *aByteConsumed)
*
* @brief This function parses NAL unit and returns corresponding status code
*
* @param  aGstNAL            [IN] NAL unit structure
* @param  H265InputStreamNAL [IN] Bit stream data which is to be decoded.
* @param  aByteConsumed      [IN] bytes consumed
* @return Status code
*
*      SC_PP_SUCCESS
******************************************************************************
*/

tPPResult gH265_ParseNALUnitFrame( tPPGstNAL *pGstNAL,
                                  tPPInput_BitStream  H265InputStreamNAL,
                                  tPPi32 *aByteConsumed)
{
    tPPi32 nPrefix;
    tPPi32 nNaluLen;
    tPPu8  *apBitStream;
    tPPi32 *apBitStreamLen;
    tPPi32 nBytesConsumed = 0;
    tPPResult nRetVal;

    apBitStream    = H265InputStreamNAL.nBitStream;
    apBitStreamLen = &(H265InputStreamNAL.nBufLength);
    pGstNAL->NALId = 0;

    do
    {
        /* Determine the boundaries of one complete NAL unit */
        nRetVal = sH265_FindNalUFrame(pGstNAL ,apBitStream, apBitStreamLen,
            (tPPu32*)&nPrefix, (tPPu32*)&nNaluLen);

        if(nRetVal == SC_PP_NEXT_FRAME_FOUND)
        {
            *aByteConsumed = nBytesConsumed;
            break;
        }
        else if(nRetVal != SC_PP_SUCCESS)
        {
            *aByteConsumed = nBytesConsumed;
            break;
        }
        nBytesConsumed += (nPrefix + nNaluLen);
        pGstNAL->NALId ++;

        (*apBitStreamLen) -= (nPrefix + nNaluLen);
        apBitStream += (nPrefix + nNaluLen);
    } while(1);

    return nRetVal;
}
