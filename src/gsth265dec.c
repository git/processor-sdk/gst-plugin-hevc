/*
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>

#include <gst/video/gstvideopool.h>
#include "gsth265dec.h"
#include <malloc.h>
#include "gsth265utils.h"
#include "gsth265buf.h"
#ifdef PROFILE
#include <sys/time.h>
#endif

GST_DEBUG_CATEGORY_STATIC (gst_h265dec_debug);
#define GST_CAT_DEFAULT gst_h265dec_debug

#define H265_MAX_WIDTH      1920
#define H265_MAX_HEIGHT     1080
/* Global variables */
volatile guint32 nDecFrmNo;
volatile gint32 nFrameDecodeStart ;
volatile gint32 nFrameDecodeEnd;
volatile gint32 nTerminateStatus ;
volatile guint32 nFrameSync ;
volatile guint32 nFrameSyncSlave ;
volatile guint32 nFlushCall ;
volatile static guint8 nCnt ;
volatile guint32 nDecodeEnd ;
volatile static guint32 nDecodeComplete ;
guint32  nValidBytesLeft ;
guint32  nNumThreads ;
guint8   nGlobalDataBuffer[((H265_MAX_WIDTH*H265_MAX_HEIGHT*3 )>>1) + 40960];
guint32  nDispFrmNo ;
gint32   nTOTAL_FRAME_SIZE;
tPP_H265_DynamicParams      h265_DynamicParams;
tPPDecParam_Status          apVal ;
tPPBaseDecoder              *h265dec = NULL;
BUFFMGR_buffEleHandle       H265_dec_BuffEle ;
tPPInFrame_Buff             H265_dec_InBuff;
tPPOutFrame_Buff            H265_dec_OutBuff;
tPPInput_BitStream          H265InputStream ;
tPPInput_BitStream          H265InputStreamNAL ;
tPP_H265_CreateParams       h265_CreateParams;
tPPQueryMemRecords          ReqMemTab[PP_H265DEC_MAX_MEMTAB];
tPPYUVPlanarDisplayFrame    pOutBuf[PP_H265VDEC_MAX_REF_FRAMES + 1];
tPPInGst_Buff               GstBufOut[PP_H265VDEC_MAX_REF_FRAMES + 1];
GstMapInfo                  info_out;
tPPGstNAL                   aGstNAL;
/* Thread identifiers */
pthread_t                   thread[4];
gint32                      *pThreadRetVal[3] ;
gint32                      taskid[4] = {0};
guint8  pTempData[((H265_MAX_WIDTH*H265_MAX_HEIGHT*3 )>>1)];

#ifdef PROFILE
struct timeval tProcessSt, tProcessEd;
unsigned long long process_time ;
unsigned long long total_time ;
float frame_rate;
unsigned int nFrmNo;
#endif

/* Id numbers for each thread */
/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT,
  PROP_THREADS
};

/* the capabilities of the inputs and outputs.
*
* describe the real formats here.
*/
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
                                                                    GST_PAD_SINK,
                                                                    GST_PAD_ALWAYS,
                                                                    GST_STATIC_CAPS ("ANY")
                                                                    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
                                                                   GST_PAD_SRC,
                                                                   GST_PAD_ALWAYS,
                                                                   GST_STATIC_CAPS ("ANY")
                                                                   );                                                             

#define gst_h265dec_parent_class parent_class
G_DEFINE_TYPE (Gsth265dec, gst_h265dec, GST_TYPE_ELEMENT);

static void gst_h265dec_set_property (GObject * object, guint prop_id,
                                      const GValue * value, GParamSpec * pspec);

static void gst_h265dec_get_property (GObject * object, guint prop_id,
                                      GValue * value, GParamSpec * pspec);

static gboolean gst_h265dec_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_h265dec_chain (GstPad * pad,
                                        GstObject * parent, GstBuffer * buf);
static void*
gst_h265dec_process_call(void *pInputStream);

void gH265_InterleaveChroma_armv7(guint8 *Indata,
                                  guint8 *OutData , gint32 nWidth,
                                  gint32 nHeight);

/* initialize the h265dec's class */
static void
gst_h265dec_class_init (Gsth265decClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_h265dec_set_property;
  gobject_class->get_property = gst_h265dec_get_property;

  g_object_class_install_property (gobject_class, PROP_SILENT,
    g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
    FALSE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_THREADS,
    g_param_spec_int ("threads", "Threads", "Number of threads",
    1, 4, 4, G_PARAM_READWRITE));
  gst_element_class_set_details_simple(gstelement_class,
    "H265 Decoder ",
    "Codec/Decoder/Video",
    "Decodes H265 streams",
    "pathpartner <<pathpartner@pathpartner.org>>");

  gst_element_class_add_pad_template (gstelement_class,
    gst_static_pad_template_get (&src_factory));

  gst_element_class_add_pad_template (gstelement_class,
    gst_static_pad_template_get (&sink_factory));
}

/* initialize the new element
* instantiate pads and add them to element
* set pad calback functions
* initialize instance structure
*/
static void
gst_h265dec_init (Gsth265dec * decoder)
{
  decoder->sinkpad
    = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_event_function (decoder->sinkpad,
    GST_DEBUG_FUNCPTR(gst_h265dec_sink_event));
  gst_pad_set_chain_function (decoder->sinkpad,
    GST_DEBUG_FUNCPTR(gst_h265dec_chain));
  GST_PAD_SET_PROXY_CAPS (decoder->sinkpad);
  gst_element_add_pad (GST_ELEMENT (decoder), decoder->sinkpad);
  decoder->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  GST_PAD_SET_PROXY_CAPS (decoder->srcpad);
  gst_element_add_pad (GST_ELEMENT (decoder), decoder->srcpad);
  decoder->nInitFlag = 0;
  decoder->silent = FALSE;
}

static void
gst_h265dec_set_property (GObject * object, guint prop_id,
                          const GValue * value, GParamSpec * pspec)
{
  Gsth265dec *decoder = GST_H265DEC (object);

  switch (prop_id)
  {
  case PROP_SILENT:
    decoder->silent = g_value_get_boolean (value);
    break;
  case PROP_THREADS:
    decoder->threads = g_value_get_int (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
gst_h265dec_get_property (GObject * object, guint prop_id,
                          GValue * value, GParamSpec * pspec)
{
  Gsth265dec *decoder = GST_H265DEC (object);

  switch (prop_id) {
case PROP_SILENT:
  g_value_set_boolean (value, decoder->silent);
  break;

case PROP_THREADS:
  g_value_set_int (value , decoder->threads);
  break;

default:
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  break;
  }
}

/* GstElement vmethod implementations */

/* this function handles sink events */
static gboolean
gst_h265dec_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  gboolean ret;
  Gsth265dec *decoder;

  decoder = GST_H265DEC (parent);

  switch (GST_EVENT_TYPE (event)) {
case GST_EVENT_CAPS:
  {
    GstCaps * caps;

    gst_event_parse_caps (event, &caps);
    /* do something with the caps */

    /* and forward */
    ret = gst_pad_event_default (pad, parent, event);
    break;
  }
default:
  ret = gst_pad_event_default (pad, parent, event);
  break;
  }
  return ret;
}
/*
******************************************************************************
* @fn sPP_H265_MultiThreadInit(tPPi32 argc, tPPi8 *argv[])
*
* @brief Function to initialize multi thread parameters
*
* @param nMultiThreadParam [IN] Structure
*
* @return Number of bytes written to the file
******************************************************************************
*/
static gint32 sPP_H265_MultiThreadInit(tPPMultiThreadParams *nMultiThreadParam,
                                       guint32 nThreadID,
                                       guint32 nNumThd)
{
  /*Task ID*/
  if(nThreadID == 0)
    nMultiThreadParam->nTaskID = kH265_TASK_MASTER ;
  else
    nMultiThreadParam->nTaskID = kH265_TASK_SLAVE ;
  /*Number of threads*/
  nMultiThreadParam->nNumThreads = nNumThd ;
  /*Thread ID */
  nMultiThreadParam->nThreadID = nThreadID ;

  return GST_FLOW_OK ;
}

static GstCaps *
gst_h265dec_getcaps (GstPad * pad)
{
  GstCaps *caps = NULL;
  caps = gst_pad_get_current_caps (pad);
  if (caps == NULL) {
    GstCaps *fil = gst_pad_get_pad_template_caps (pad);
    g_print("gst_pad_get_current_caps Failing\n");
    return gst_caps_copy (fil);
  } else {
    return gst_caps_copy (caps);
  }
}

/* chain function
* this function does the actual processing
*/
static GstFlowReturn
gst_h265dec_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  Gsth265dec *decoder;
  GstMapInfo info ;
  guint32 taskidMaster = 0;
  gint32 nRetVal;
  gint32 aByteConsumed = 0;
  gint32 nOffset = 0;
  gint32 i;
  gint32 nFlag = 0;
  guint32 nIndex ,nIndex1;
  guint32 nNumMemTabEntries =0;
  guint32 nRet;
  guint32 nDispFlag;
  GstQuery *query = NULL;
  guint min = 0;
  guint max = 0;
  guint size = 0;
  GstCaps *Caps;
  GstStructure *config;
  GstFlowReturn flow;
  GstBuffer *pVpeBuff = NULL;
  GstBuffer *GstOutBufTemp;

  decoder = GST_H265DEC (parent);

  gst_buffer_map(buf, &info , GST_MAP_READ);

  H265InputStream.nBufLength    = info.size;
  H265InputStreamNAL.nBufLength = info.size;

  nOffset = nValidBytesLeft;
  nDispFlag = 1;
  /* Initialiazations */
  if(!decoder->nInitFlag)
  {
    /* H265 decoder initialization*/
    FUNC_MEMSET(ReqMemTab, 0,
      sizeof(tPPQueryMemRecords)*PP_H265DEC_MAX_MEMTAB);

    nRetVal = 0;
    h265_CreateParams.nNumThreads = decoder->threads;
    h265_CreateParams.nMaxWidth   = H265_MAX_WIDTH;
    h265_CreateParams.nMaxHeight  = H265_MAX_HEIGHT;
    nNumThreads = decoder->threads;
    decoder->nTimeStamp = 0;

    nRetVal = gPP_H265_QueryMemoryRequirements( &ReqMemTab[0],
      &h265_CreateParams, &nNumMemTabEntries);

    /* Increase default malloc size */
    mallopt(M_MMAP_THRESHOLD , (1024*1024*30));

    /*Allocate the memory*/
    nRetVal = MEMMGR_AllocMemoryRequirements(&ReqMemTab[0],
      nNumMemTabEntries);

    if(nRetVal != GST_FLOW_OK)
    {
      g_print("Error in Allocating the memory requirements.\n");
    }

    /*Pass the memory and Initialize the decoder*/
    nRetVal = gPP_H265_InitializeDecoder  (&h265dec,
      &ReqMemTab[0],&h265_CreateParams);

    h265_DynamicParams.nDecodeHeader = PP_H265_PARSE_HEADER;

    if(nRetVal != GST_FLOW_OK)
    {
      g_print("Error in Initializing \n");
    }

    /* Initialise pLum pointers in output base frames to NULL.
    * When the decoder initialises these pointers with valid values we know
    * that its time to dump/display them
    */
    nIndex = 0;
    while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES+1) )
    {
      pOutBuf[nIndex].pLum = NULL;
      nIndex++;
    }

    for(i = 1; i < nNumThreads; i++)
    {
      taskid[i] = i;
      /* Create a thread with its argument in taskid[i] */
      nRet = pthread_create(&thread[i], NULL,
        &gst_h265dec_process_call, &taskid[i]);
      if (nRet)
      {
        /* Check for errors */
        printf("ERROR; return code from pthread_create() is %d\n",
          nRet);
      }
    }
    if(!gst_pad_has_current_caps(decoder->srcpad))
      g_print("gst_pad_has_current_caps Failing\n");

    Caps = gst_h265dec_getcaps (decoder->srcpad);
    if(Caps == NULL)
      g_print("gst_pad_get_current_caps Failing\n");


    query =
      gst_query_new_allocation ( Caps, TRUE);

    if (gst_pad_peer_query (decoder->srcpad, query))
    {
      g_print("gst_pad_peer_query sucessful\n");
    }
    else
    {
      g_print("gst_pad_peer_query failed\n");
    }

    if (gst_query_get_n_allocation_pools (query) > 0) {
      /* we got configuration from our peer, parse them */
      gst_query_parse_nth_allocation_pool (query, 0, &decoder->externalpool, &size,
        &min, &max);
    }
    else
    {
      decoder->externalpool = NULL;
      size = 0;
      min = max = 0;
    }

    if (decoder->externalpool == NULL) {
      /* we did not get a pool, make one ourselves then */
      decoder->externalpool = gst_video_buffer_pool_new ();
    }

    config = gst_buffer_pool_get_config (decoder->externalpool);
    gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);
    gst_buffer_pool_config_set_params (config, Caps, size, min, max);
    gst_buffer_pool_set_config (decoder->externalpool, config);

    /* and activate */
    gst_buffer_pool_set_active (decoder->externalpool, TRUE);
    decoder->nInitFlag = 1;
  }

  for(i = 0 ; i < info.size ;i++)
  {
    nGlobalDataBuffer[i + nOffset] = info.data[i];
  }

  H265InputStream.nBitStream =
    H265InputStreamNAL.nBitStream =  &nGlobalDataBuffer[0];

  H265InputStreamNAL.nBufLength = (info.size) + nOffset;
  /* Default buffer size*/
  if(info.size < 4096 )
  {
    nFlag = 1;
  }

  do
  {
    aByteConsumed = 0;
    /* Parse frame data*/
    nRetVal = gH265_ParseNALUnitFrame(&aGstNAL,
      H265InputStreamNAL , &aByteConsumed);

    if(nRetVal == SC_PP_NEXT_FRAME_FOUND )
    {
      H265InputStream.nBufLength = aByteConsumed;
      H265InputStream.nBitStream = H265InputStreamNAL.nBitStream;
      /* if frame data decode */
      gst_h265dec_process_call(&taskidMaster);

      H265InputStreamNAL.nBitStream += aByteConsumed;
      H265InputStreamNAL.nBufLength -= aByteConsumed;

    }
    else if(nFlag)
    {
      H265InputStream.nBufLength = aByteConsumed;
      H265InputStream.nBitStream = H265InputStreamNAL.nBitStream;
      /* if frame data decode */
      gst_h265dec_process_call(&taskidMaster);

      /* File sink */
      nIndex = 0;

      while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
      {
        if (pOutBuf[nIndex].pLum)
        {
          nIndex1 =0;
          while(nIndex1 < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
          {
            if(GstBufOut[nIndex1].GstDispBuf == NULL)
            {
              nIndex1++;
              continue;
            }

            if( pOutBuf[nIndex].pLum == GstBufOut[nIndex1].Out_map.data)
            {
              /* Conversion of YUV data to NV12 format */
              gH265_InterleaveChroma_armv7(GstBufOut[nIndex1].\
                Out_map.data, pTempData, apVal.nPicWidth,
                apVal.nPicHeight);

              flow = gst_buffer_pool_acquire_buffer (decoder->externalpool, &pVpeBuff, NULL);

              if (flow != GST_FLOW_OK)
              {
                GST_INFO_OBJECT (decoder, "couldn't allocate output buffer, flow %s",
                  gst_flow_get_name (flow));
              }
              gst_buffer_fill(pVpeBuff, 0, pTempData,
                ((nTOTAL_FRAME_SIZE * 3) >> 1));

              nRet = gst_pad_push (decoder->srcpad,
                pVpeBuff);

              nDispFrmNo++;
              pOutBuf[nIndex].pLum = NULL;
              break;
            }
            nIndex1++;
          }
        }
        else
        {
          break;
        }
        nIndex++;
      }
      /*Call release buffer*/
      BUFFMGR_ReleaseBufID(&H265_dec_OutBuff.bufId[0], GstBufOut);

      nFlushCall = 1;
      gst_h265dec_process_call(&taskidMaster);

      while(nDecodeComplete != (nNumThreads - 1) );
      /* Deallocate the memory*/
      MEMMGR_DeAllocMemory(&ReqMemTab[0]);

      for(i = 0; i < (nNumThreads - 1); i++)
      {
        pthread_join(thread[(i + 1)] ,
          (void **)&pThreadRetVal[i]);
      }
      H265InputStreamNAL.nBitStream += aByteConsumed;
      H265InputStreamNAL.nBufLength -= aByteConsumed;
    }
    else
    {
      for(i= 0 ; i< H265InputStreamNAL.nBufLength ;i++ )
      {
        nGlobalDataBuffer[i] = H265InputStreamNAL.nBitStream [i];
      }
      nValidBytesLeft = H265InputStreamNAL.nBufLength;
      break;
    }

    /* File sink */
    nIndex  = 0;

    while( nIndex < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
    {
      if (pOutBuf[nIndex].pLum)
      {
        nIndex1 =0;
        while(nIndex1 < (PP_H265VDEC_MAX_REF_FRAMES + 1) )
        {
          if(GstBufOut[nIndex1].GstDispBuf == NULL)
          {
            nIndex1++;
            continue;
          }

          if( pOutBuf[nIndex].pLum == GstBufOut[nIndex1].Out_map.data)
          {
            /* Conversion of YUV data to NV12 format */
            gH265_InterleaveChroma_armv7(GstBufOut[nIndex1].\
              Out_map.data, pTempData, apVal.nPicWidth,
              apVal.nPicHeight);

            flow = gst_buffer_pool_acquire_buffer (decoder->externalpool, &pVpeBuff, NULL);

            if (flow != GST_FLOW_OK)
            {
              GST_INFO_OBJECT (decoder, "couldn't allocate output buffer, flow %s",
                gst_flow_get_name (flow));
            }

            gst_buffer_fill(pVpeBuff, 0, pTempData,
              ((nTOTAL_FRAME_SIZE * 3) >> 1));

            nRet = gst_pad_push (decoder->srcpad,
              pVpeBuff);

            nDispFrmNo++;
            pOutBuf[nIndex].pLum = NULL;
            break;
          }
          nIndex1++;
        }
      }
      else
      {
        break;
      }
      nIndex++;
    }
    /*Call release buffer*/
    BUFFMGR_ReleaseBufID(&H265_dec_OutBuff.bufId[0], GstBufOut );

  }while(H265InputStreamNAL.nBufLength > 3);

  /*unmapping input buffer(freeing)*/
  gst_buffer_unmap(buf, &info);

  if(nFlag && nDispFlag)
  {
#ifdef PROFILE
    g_print(" Total decode time               : %lld.%lld ms\n",
      total_time/1000,total_time%1000);
    frame_rate = (nDecFrmNo * (1000000))/(float)total_time;
    g_print(" Frames per second               : %0.2f fps\n",frame_rate);
#endif
    nDispFlag = 0;
  }
  return 0;

}

static void*
gst_h265dec_process_call(void* thread_args)
{
  gint32 nRetVal ,nRetV;
  gint32 *thread_ptr = (gint32 *)thread_args;
  gint32 thread_no = *thread_ptr;
  /*MultiThread*/
  tPPMultiThreadParams nMultiThreadParam;
  guint32  BuffID;

  nRetVal = sPP_H265_MultiThreadInit(&nMultiThreadParam ,
    thread_no, nNumThreads);

  if(nRetVal != SC_PP_SUCCESS)
  {
    printf("Invalid Multi thread init params\n");
    goto EXIT_H265DEC;
  }
START_FRAME:
  /* Multithread synchronization*/
  gPP_PPL_AtomicAdd8((guint8 *)&nFrameSyncSlave, 1);
  while((nFrameSyncSlave % nNumThreads) != 0) ;

  if(!nFlushCall)
  {
    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
      /* h265_DynamicParams.nDecodeHeader = PP_H265_PARSE_HEADER;*/

      nRetV = h265dec->vSet(h265dec, PP_SET_DECODE_HDR,
        h265_DynamicParams.nDecodeHeader, &h265_DynamicParams);

      if(nRetV == EC_PP_FAILURE)
      {
        nTerminateStatus = 1;
        goto EXIT_H265DEC;
      }
      if(h265_DynamicParams.nDecodeHeader == PP_H265_PARSE_HEADER)
      {
        guint8 *pBitStream ;
        gint32 nBitLen;

        pBitStream = H265InputStream.nBitStream ;
        nBitLen    = H265InputStream.nBufLength ;
        nRetVal = h265dec->vDecode(h265dec,
          &(H265InputStream),  &(H265_dec_InBuff),
          &(H265_dec_OutBuff),
          (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]),
          &nMultiThreadParam);

        H265InputStream.nBitStream = pBitStream +
          (nBitLen - H265InputStream.nBufLength);

        nRetVal =  h265dec->vGet(h265dec,
          PP_GET_PARAMSTATUS, (void *)(&apVal));
      }

      h265_DynamicParams.nDecodeHeader = PP_H265_DECODE_ACCESSUNIT;

      nRetV = h265dec->vSet(h265dec, PP_SET_DECODE_HDR,
        h265_DynamicParams.nDecodeHeader, &h265_DynamicParams);

      if(h265_DynamicParams.nDecodeHeader ==
        PP_H265_DECODE_ACCESSUNIT)
      {
        BuffID =  BUFFMGR_GetFreeBufID(GstBufOut);

        nTOTAL_FRAME_SIZE =(apVal.nPicWidth * apVal.nPicHeight);

        /* Assign the buffer allotted by Buffer manager to I/p frame*/
        if(BuffID < (PP_H265VDEC_MAX_REF_FRAMES+1))
        {
          GstBufOut[BuffID - 1].GstDispBuf =
            gst_buffer_new_allocate(NULL,
            ((nTOTAL_FRAME_SIZE * 3) >>1), NULL);
          if(GST_IS_BUFFER(GstBufOut[BuffID - 1].GstDispBuf))
          {
            gst_buffer_map(GstBufOut[BuffID - 1].GstDispBuf,
              &GstBufOut[BuffID - 1].Out_map , GST_MAP_WRITE) ;
          }
          (H265_dec_InBuff.buf[0])=
            GstBufOut[BuffID - 1].Out_map.data;
          (H265_dec_InBuff.buf[1])=
            (H265_dec_InBuff.buf[0])+ nTOTAL_FRAME_SIZE;
          (H265_dec_InBuff.buf[2])=
            (H265_dec_InBuff.buf[1]) + (nTOTAL_FRAME_SIZE >>2);
          (H265_dec_InBuff.bufId) = BuffID;
        }
        else
        {
          printf("No Free Buffers available\n");
          nTerminateStatus = 1;
        }
      }
    }
    gPP_PPL_AtomicAdd8((guint8 *)&nFrameSync, 1);

    while((nFrameSync % nNumThreads) != 0)
    {
      if(nTerminateStatus == 1)
      {
        goto EXIT_H265DEC;
      }
    };
    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
#ifdef PROFILE
      gettimeofday(&tProcessSt , NULL) ;
#endif
    }
    nRetVal = h265dec->vDecode(h265dec,
      &(H265InputStream),  &(H265_dec_InBuff),
      &(H265_dec_OutBuff), (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]),
      &nMultiThreadParam);

    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
#ifdef PROFILE
      gettimeofday(&tProcessEd , NULL) ;
      process_time = (tProcessEd.tv_sec - tProcessSt.tv_sec)*1000000 +
        (tProcessEd.tv_usec - tProcessSt.tv_usec);
      total_time += process_time;
#endif
    }
    if(nRetVal == EC_PP_FAILURE)
    {
      if(nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
      {
        goto EXIT_H265DEC;
      }
    }
    if(nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
      nCnt    = 0;
      nFrameSyncSlave = 0;
      nFrameSync      = 0;

      h265dec->vGet( h265dec,
        PP_GET_ERRORSTATUS,(void *)(&apVal));

      if(nDecFrmNo >= 0)
      {
        gint32 nTemp =0;
        h265dec->vGet( h265dec, PP_GET_BUFSTATUS,
          (void *)(&nTemp));
        if((nTemp == BUFFMGR_FREE) || (nTemp == BUFFMGR_USED))
        {
          if(h265_DynamicParams.nDecodeHeader ==
            PP_H265_DECODE_ACCESSUNIT)
          {
            GstBufOut->bufStatus = (BUFFMGR_BufferStatus)nTemp;
          }
        }
      }

      /* If the end of NAL unit(Start of the next NAL unit)
      not found exit*/
      if( nRetVal == EC_PP_H265_NO_NALU_END_FOUND )
      {
        printf("\nError : NAL unit's end not found\n");
        goto  EXIT_H265DEC;
      }

      if ( ( nRetVal == EC_PP_FAILURE )
        ||( nRetVal ==  EC_PP_INVALID_PARAM ) ||
        (nRetVal == EC_PP_NOT_SUPPORTED) )
      {
        printf(" Invalid parameters\n");
      }
      /* If complete frame is decoded, write that in the output file*/
      if( nRetVal == SC_PP_SUCCESS || nRetVal == SC_PP_RESOLUTION_CHANGED
        ||  nRetVal == SC_PP_END_OF_PICTURE)
      {
        nDecFrmNo++;
      }/*end of if( nRetVal == SC_PP_SUCCESS )*/
    }/*end of if(nMultiThreadParam.nTaskID == kH265_TASK_MASTER)*/

    if (nMultiThreadParam.nTaskID != kH265_TASK_MASTER)
    {
      if (( nRetVal == EC_PP_FAILURE ) ||
        (nRetVal ==  EC_PP_INVALID_PARAM ) ||
        (nRetVal == EC_PP_NOT_SUPPORTED) ||
        (nRetVal == EC_PP_H265_NO_NALU_END_FOUND))
      {
        goto EXIT_H265DEC ;
      }

      gPP_PPL_AtomicAdd8((guint8 *)&nFrameDecodeStart,1);
      do
      {
        if(nFrameDecodeStart == (nNumThreads -1))
        {
          gPP_PPL_AtomicAdd8((guint8 *)&nFrameDecodeEnd,1);
          break;
        }
      }while(1);
    }
    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
      do
      {
        if(nFrameDecodeEnd == (nNumThreads -1))
        {
          nFrameDecodeStart = 0;
          nFrameDecodeEnd = 0;
          break;
        }
      }
      while(1);
    }
    else
    {
      while(nFrameDecodeStart);
    }

    if (nMultiThreadParam.nTaskID != kH265_TASK_MASTER)
    {
      goto START_FRAME;
    }
  }
  else
  {
    /*Flush Call*/
    if (nMultiThreadParam.nTaskID == kH265_TASK_MASTER)
    {
      /* after Decoding Last Frame Dump all the rest frame */
      do
      {
        h265dec->vSet(h265dec ,
          PP_SET_DPB_FLUSH , PP_TRUE, &h265_DynamicParams);

        nRetVal = h265dec->vDecode(h265dec,
          &(H265InputStream),
          &(H265_dec_InBuff),
          &(H265_dec_OutBuff),
          (tPPYUVPlanarDisplayFrame *)(&pOutBuf[0]),
          &nMultiThreadParam);

      }while (nRetVal == SC_PP_SUCCESS);
    }
  }
EXIT_H265DEC:
  {
    if (nMultiThreadParam.nTaskID != kH265_TASK_MASTER)
    {
      gPP_PPL_AtomicAdd8((guint8 *)&nDecodeComplete,1);
      pthread_exit(&nMultiThreadParam.nThreadID);
    }
    /* Multithread synchronization*/
  }
  return GST_FLOW_OK;
}

/* entry point to initialize the plug-in
* initialize the plug-in itself
* register the element factories and other features
*/
static gboolean
h265dec_init (GstPlugin * h265dec)
{
  /* debug category for fltering log messages
  *
  * exchange the string 'Template h265dec' with your description
  */
  GST_DEBUG_CATEGORY_INIT (gst_h265dec_debug, "h265dec",
    0, "H265dec decoder");

  return gst_element_register (h265dec, "h265dec", GST_RANK_NONE,
    GST_TYPE_H265DEC);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
* in configure.ac and then written into and defined in config.h, but we can
* just set it ourselves here in case someone doesn't use autotools to
* compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
*/
#ifndef PACKAGE
#define PACKAGE "myfirsth265dec"
#endif

/* gstreamer looks for this structure to register h265decs
*
* exchange the string 'Template h265dec' with your h265dec description
*/
GST_PLUGIN_DEFINE (
                   GST_VERSION_MAJOR,
                   GST_VERSION_MINOR,
                   h265dec,
                   "H265dec decoder",
                   h265dec_init,
                   "1.0.0",
                   "BSD",
                   "GStreamer",
                   "http://gstreamer.net/"
                   )
